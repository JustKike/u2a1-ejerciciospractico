function cuentaAtras() {
    var contador = 10;

    function restar() {
        console.log(contador);
        contador = contador - 1;
        if (contador == 0) {
            acabarCuentaAtras();
        }
    }

    function acabarCuentaAtras() {
        clearInterval(temporizador); // Paramos la ejecución del método, indicando la variable creada al final
        console.log("Fin del tiempo, ha llegado a cero");
    }
    var temporizador = setInterval(function() {
        restar();
    }, 1000); // 10000ms = 10s
}

var contador = 0;
var contar = function() {
    contador++;
    console.log(contador);
    if (contador === 6) {
        clearInterval(intervalo);
        console.log("Ahora vamos a contar hacia atras");
        cuentaAtras();
    }
};
// setTimeout(saludo, 3000);
var intervalo = setInterval(contar, 1000);