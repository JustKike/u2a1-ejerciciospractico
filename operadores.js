//Aritmeticos
let a = 4,
    b = 2;

function suma(a, b) {
    result = a += b;
    return result;
}

function resta(a, b) {
    result = a -= b;
    return result;
}

function division(a, b) {
    result = a /= b;
    return result;
}

function multiplicacion(a, b) {
    result = a *= b;
    return result;
}

function residuo(a, b) {
    result = a %= b;
    return result;
}

function exponenciacion(a, b) {
    result = a **= b;
    return result;
}

//Operadores Logicos
function negacion(a, b) {
    result = a != b;
    return result;
}

function And(a, b) {
    result = (a == 4) && (b == 2);
    return result;
}

function disyuncion(a, b) {
    result = (a == 8) || (b == 2);
    return result;
}

function exclusion(a, b) {
    result = (a == 8) ^ (b == 2);
    return result;
}

//Operadores Relacionales
function igual(a) {
    result = (a == "4");
    return result;
}

function igualEstricta(a) {
    result = (a === "4");
    return result;
}

function desigualdad(a, b) {
    result = (a != b);
    return result;
}

function desigualdadE(a) {
    result = (a !== "4");
    return result;
}

function mayorQue(a, b) {
    result = (a > b);
    return result;
}

function mayorIgual(a) {
    result = (a >= 4);
    return result;
}

function menorQue(a, b) {
    result = (a < b);
    return result;
}

function menorIgual(b) {
    result = (b <= 2);
    return result;
}

console.log("\nOperadores Aritmeticos");
console.log("Variable a = ", a);
console.log("Variable b = ", b);
console.log("---------------------------");
console.log("Suma: a + b =", suma(a, b));
console.log("---------------------------");
console.log("Resta: a - b =", resta(a, b));
console.log("---------------------------");
console.log("Division: a / b =", division(a, b));
console.log("---------------------------");
console.log("Multiplicacion: a * b =", multiplicacion(a, b));
console.log("---------------------------");
console.log("Residuo: a % b =", residuo(a, b));
console.log("---------------------------");
console.log("exponenciación: a ** b =", exponenciacion(a, b));

console.log("\nOperadores Logicos");
console.log("---------------------------");
console.log("Negacion !(NOT): a != b :", negacion(a, b));
console.log("---------------------------");
console.log("Conjunción lógica && (And): a = 4 && b = 2 :", And(a, b));
console.log("---------------------------");
console.log("Disyunción lógica || (Or): a = 8 || b = 2 :", disyuncion(a, b));
console.log("---------------------------");
console.log("Exclusión lógica ^ (Xor): a = 8 ^ b = 2 :", exclusion(a, b));

console.log("\nOperadores Relacionales");
console.log("---------------------------");
console.log("Igualdad: a == '4' :", igual(a));
console.log("---------------------------");
console.log("Igualdad estrictacta: a === '4' :", igualEstricta(a));
console.log("---------------------------");
console.log("Desigualdad: a != b :", desigualdad(a, b));
console.log("---------------------------");
console.log("Desigualdad estricta: a !== '4' :", desigualdadE(a));
console.log("---------------------------");
console.log("Mayor que (>): a > b :", mayorQue(a, b));
console.log("---------------------------");
console.log("Mayor o igual que (>=): a >= 4 :", mayorIgual(a));
console.log("---------------------------");
console.log("Menor que (<): a < b :", menorQue(a, b));
console.log("---------------------------");
console.log("Menor o igual que (<=): b <= 2 :", menorIgual(b));