var zoologico = ["Ganzo", "Perro", "Gato", "Araña"];

function ListaOriginal(lista) {
    lista.forEach(function(elemento, indice) {
        console.log(indice + 1, elemento);
    });
}

function OrdenarLista(lista) {
    lista.sort();
    lista.forEach(function(elemento, indice) {
        console.log(indice + 1, elemento);
    });
}

function añadirFinal(lista, animal) {
    lista.push(animal);
    lista.forEach(function(elemento, indice) {
        console.log(indice + 1, elemento);
    });
}

function añadirInicio(lista, animal) {
    lista.unshift(animal);
    lista.forEach(function(elemento, indice) {
        console.log(indice + 1, elemento);
    });
}

function deleteInicio(lista) {
    lista.shift();
    lista.forEach(function(elemento, indice) {
        console.log(indice + 1, elemento);
    });
}

function deleteFinal(lista) {
    lista.pop();
    lista.forEach(function(elemento, indice) {
        console.log(indice + 1, elemento);
    });
}

function buscarIndice(lista, animal) {
    console.log(lista.indexOf(animal) + 1);
}

function filtrarLista(lista, dato) {
    const filterItems = query => {
        return lista.filter((el) =>
            el.toLowerCase().indexOf(query.toLowerCase()) > -1
        );
    };
    console.log(filterItems(dato));
}

function swap(lista, val1, val2) {
    if (!lista.includes(val1) || !lista.includes(val2)) return;
    let val1_index = lista.indexOf(val1);
    let val2_index = lista.indexOf(val2);
    lista.splice(val1_index, 1, val2);
    lista.splice(val2_index, 1, val1);
    lista.forEach(function(elemento, indice) {
        console.log(indice + 1, elemento);
    });
}

//salida por pantalla
console.log("\nImprimimos la lista orginal:");
ListaOriginal(zoologico);
console.log("\nOrdenamos la lista:");
OrdenarLista(zoologico);
console.log("\nImprimimos el tamaño de la lista:");
console.log(zoologico.length);
console.log("\nBuscamos el primer elemento de la lista ordenada:");
console.log(zoologico[0]);
console.log("\nAgregamos un animal al final de la lista:");
añadirFinal(zoologico, "Escorpion");
console.log("\nAgregamos un animal al inicio de la lista:");
añadirInicio(zoologico, "Tortuga");
console.log("\nEliminamos un animal al inicio de la lista:");
deleteInicio(zoologico);
console.log("\nEliminamos un animal al final de la lista:");
deleteFinal(zoologico);
console.log("\nBuscamos el indice del animal 'Gato' en la lista:");
buscarIndice(zoologico, "Gato");
console.log("\nFiltramos los animales que comiencen con la letra G:");
filtrarLista(zoologico, "G");
console.log("\nMovemos de posicion Perro por Araña:");
swap(zoologico, "Araña", "Perro");